# GPS-NEO-6M

Receive [NMEA 0183](https://en.wikipedia.org/wiki/NMEA_0183) telegrams from a NEO-6M GPS module.

**HAL_UART_Receive_IT** is used for receiving the telegram char by char.
**HAL_UART_RxCpltCallback** assembles telegrams from the received chars. 
A rudimentary parser is implemented, parsing GPRMC telegrams.
The output is sent to the STLINK VCP (USART2) using **HAL_UART_Transmit_DMA**.

See https://www.u-blox.com for more info on the NEO-6M module and GPS NMEA telegrams.